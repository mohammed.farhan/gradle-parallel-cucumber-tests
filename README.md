# Gradle Parallel Cucumber Tests

This plugin enables you to run cucumber tests in parallel. The configuration exposes the main class used for the test execution. So you are able to configure different bdd command line implementations like plain cucumber or serenity with cucumber. It searches for feature files in the specified path and starts every feature file (or even single scenarios) in a separate jvm.  


## How to integrate in your gradle project

Take a look at the integration tests if something is unclear. You need to to add following lines to your build.gradle:
```
apply plugin: "com.gitlab.et.paralleltests"
```
```
buildscript {

    ...
    
    dependencies {
        ...
        classpath("com.gitlab.et.paralleltests:gradle-parallel-cucumber-tests:+")
    }
}

```



## Configuration of the plugin
In your build.gradle you can configure following options:

*    __printConfiguration__: prints these configuration to standard out -  default: false
*    __parallelExecutions__: number of parallel tests (running each in an own jvm) - default: 1
*    __cucumberCliClass__: the cli class to use - default: ```cucumber.api.cli.Main```. For a more recent cucumber version use ```io.cucumber.core.cli.Main``` instead.
*    __properties__: custom properties you want to pass to the tests - default empty
*    __glue__: package of the glue code - default empty
*    __featureDir__: folder containing all feature files to execute - default empty
*    __tags__: run only specific tags - default empty
*    __environmentVariablePrefix__: prefix to identify variables to be passed to the tests. Example: if set to ```foo``` then ```foo.webdriver.baseurl=VALUE``` is passed as ```webdriver.baseurl=VALUE``` - default empty
*    __strict__: fails if unimplemented steps are found - default: false
*    __printTestOutput__: prints output of the testcases - default: true
*    __parallelScenarios__: enables running per Scenario - default: false
*    __shortenPathForLogging__: reduces the path for the test names by their highest possible equality. 
*    __cucumberPlugins__: list of cucumber plugins which will be activated - default empty 

An example can be found in the integration tests build.gradle:
```
testParallel{
    parallelExecutions = 5
    cucumberCliClass = 'cucumber.api.cli.Main'
    glue ="sr.dv.fr"
    featureDir= "src/test/resources/features/"
    printConfiguration = true
    strict = true
    printTestOutput = true
    tags = "not @ignore"
    environmentVariablePrefix = "foo"
    parallelScenarios = false
    cucumberPlugins = [
            "json:ReportFolderParallel/cucumber.json",
            "junit:ReportFolderParallel/junit.xml"
    ]
}

```
### Overriding configuration in the command line

Besides this static configuration in the build.gradle, you also have the possibility to override them directly in the command line executing the 
gradle task via environment variables. To override them use the prefix ```testParallel```, e.g.: ```-DtestParallel.parallelExecutions=10```.
This can be useful to have specific configurations on a build server. 
 
If you want to pass multiple ```cucumberPlugins```, you can do that by separating them via ```;``` . 

Example:

```-DtestParallel.cucumberPlugins="json:ReportFolderParallel/cucumber.json;junit:ReportFolderParallel/junit.xml"```    


## Execution of tests

Use the gradle task _testParallel_ to execute the tests:
```
./gradlew testParallel
```


If the tests running inside the test module keep in mind, that if you use the task together with the _build_ task, you possibly run the tests twice. Because by default _build_ depends on _test_


### Running scenarios in parallel

By setting the option ```parallelScenarios``` to ```true``` it's possible to run scenarios in parallel instead of complete feature files. All feature files are scanned for scenarios and queued independently of the feature. This means that scenarios of different features will run in parallel if ```parallelExecutions``` is greater then ```1```. To avoid side effects make sure that all scenarios are independent of each other and stateless. Keep in mind that ```Background```'s are executed automatically for corresponding scenarios.

This option is set to false by default because it's only helpful on long running scenarios and can cause even longer execution times. For example, if the tests are running very fast but using a framework which extends the start time (like spring), then the startup of a new jvm will take longer than maybe the test execution of all scenarios in a feature. 




### Current tested BDD command lines
* Cucumber
* Cucumber with Serenity



## Author

* **Erol Turan**

