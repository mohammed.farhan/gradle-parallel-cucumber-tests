Feature: Check Addition

  This scenarios checking that the addition works correctly


  Scenario: Add numbers to correct result 9
    Given x is set to 11
    And y is set to 11
    When the calculator adds x to y
    Then the result is 22
