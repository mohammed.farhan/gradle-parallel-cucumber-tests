Feature: Check Addition

  This scenarios checking that the addition works correctly


  Scenario: Add numbers to correct result 10
    Given x is set to 100
    And y is set to 100
    When the calculator adds x to y
    Then the result is 200
