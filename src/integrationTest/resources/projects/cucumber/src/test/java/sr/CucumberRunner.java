package sr;


import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/resources/features/",
        glue = "sr.dv.fr",
        strict = true,
        plugin = {"pretty",
                "html:ReportFolder/",
                "json:ReportFolder/cucumber.json",
                "junit:ReportFolder/cucumber.xml"}
)
public class CucumberRunner {
}
