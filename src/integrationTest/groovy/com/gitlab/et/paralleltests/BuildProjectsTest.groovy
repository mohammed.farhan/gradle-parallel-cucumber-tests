package com.gitlab.et.paralleltests

import spock.lang.Specification

class BuildProjectsTest extends Specification {

    String projectFolder = this.getClass().getResource('/projects/').toURI().getPath()
    long timeout = 180_000


    def "Cucumber with serenity build and aggregate successfully"() {
        given:
        def command = commandOf("cucumberWithSerenity")
        command.addAll("aggregate", "clearReport")
        def proc = command.execute()

        when:
        proc.waitForOrKill(timeout)

        and:
        println "--------------------------"
        println "Process exit code: ${proc.exitValue()}"
        println "Std Err: ${proc.err.text}"
        println "Std Out: ${proc.in.text}"
        println "--------------------------"
        then:
        proc.exitValue() == 0


    }

    def "Cucumber build successfully"() {
        given:
        def proc = commandOf("cucumber").execute()

        when:
        proc.waitForOrKill(timeout)

        and:
        println "--------------------------"
        println "Process exit code: ${proc.exitValue()}"
        println "Std Err: ${proc.err.text}"
        println "Std Out: ${proc.in.text}"
        println "--------------------------"
        then:
        proc.exitValue() == 0


    }

    def "Environment variables are passed correctly"() {
        given:
        def command = commandOf("cucumberEnvVar")
        command.add("-Dfoo.bar=boo")
        def proc = command.execute()

        when:
        proc.waitForOrKill(timeout)

        and:
        println "--------------------------"
        println "Process exit code: ${proc.exitValue()}"
        println "Std Err: ${proc.err.text}"
        println "Std Out: ${proc.in.text}"
        println "--------------------------"
        then:
        proc.exitValue() == 0


    }

    def "Cucumber build successfully with overriding config"() {
        given:
        def command = commandOf("cucumberOverridingConfig")
        command.addAll("-DtestParallel.parallelExecutions=5",
                "-DtestParallel.cucumberCliClass=cucumber.api.cli.Main",
                "-DtestParallel.glue=sr.dv.fr",
                "-DtestParallel.featureDir=src/test/resources/features/",
                "-DtestParallel.printConfiguration=true",
                "-DtestParallel.strict=true",
                "-DtestParallel.printTestOutput=false")
        println command
        def proc = command.execute()

        when:
        proc.waitForOrKill(timeout)

        and:
        println "--------------------------"
        println "Process exit code: ${proc.exitValue()}"
        println "Std Err: ${proc.err.text}"
        println "Std Out: ${proc.in.text}"
        println "--------------------------"
        then:
        proc.exitValue() == 0


    }


    List<String> commandOf(String projectName) {
        return ["./gradlew", "-p", "$projectFolder$projectName/", "clean", "build", "testParallel", "-xtest"]

    }

}