package com.gitlab.et.paralleltests.helper.model

class Feature {
    String path
    List<Scenario> scenarios = new ArrayList<>()


    @Override
    public String toString() {
        return "Feature{" +
                "path='" + path + '\'' +
                ", scenarios=" + scenarios +
                '}';
    }
}
