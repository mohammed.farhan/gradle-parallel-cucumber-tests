package com.gitlab.et.paralleltests.helper

import com.gitlab.et.paralleltests.ParallelTestExtension
import com.gitlab.et.paralleltests.helper.model.Feature
import spock.lang.Specification

class FeatureLoaderTest extends Specification {

    ParallelTestExtension extension = [
            glue      : 'test/glue',
            featureDir: 'src/test/resources/featureLoaderTest'
    ]

    FeatureLoader featureLoader = new FeatureLoader(extension, "")


    def "Check features are found"() {
        when:
        List<Feature> actualFeatures = featureLoader.locateFeatures()



        then:
        actualFeatures.size() == 2
        actualFeatures.path.find { it.endsWith("Addition.feature") }
        actualFeatures.path.find { it.endsWith("Multiplication.feature") }

    }


    def "Check Scenarios are loaded correctly"() {
        given:
        def expectedFeatures = [
                [featureFileName: "Addition.feature",
                 scenarios      : ["^Add x to y\$",
                                   "^Add x to y using examples table\$"]
                ],

                [featureFileName: "Multiplication.feature",
                 scenarios      : ["^Add numbers to correct result 4\$"]
                ],

        ]


        when:
        List<Feature> actualFeatures = featureLoader.locateFeatures()



        then:
        expectedFeatures.each {
            Feature f = actualFeatures.find { Feature f -> f.path.endsWith(it.featureFileName as String) }
            assert f.scenarios.name.containsAll(it.scenarios)
        }


    }

}
